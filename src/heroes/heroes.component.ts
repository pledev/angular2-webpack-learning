import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import '../../public/css/styles.css';

import Hero from '../hero/hero';
import HeroService from '../hero/hero.service';

@Component({
  selector: 'heroes',
  templateUrl: './heroes.component.html',
  styleUrls: [ './heroes.component.css' ]
})

export default class HeroesComponent implements OnInit {
  heroes: Hero[];
  selectedHero: Hero;

  constructor(
    private heroService: HeroService,
    private router: Router ) {

    }

  getHeroes(): void {
    this.heroService.getHeroes().then(heroes =>  this.heroes = heroes);
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }
}
